﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace WarZone
{
    public partial class WarWindow : Form
    {
        private const int BEGIN = 12;
        private bool begun = false;
        private bool sent = false;
        private int currentValue = -1;
        private List<PictureBox> cards = new List<PictureBox>();
        private NetworkStream clientStream;

        public WarWindow()
        {
            InitializeComponent();
            ToggleAll(false);
            InitConnection();
        }

        private void ToggleAll(bool val)
        {
            btn_forfeit.Enabled = val;
            
            foreach(var item in this.cards)
            {
                item.Enabled = val;
            }
            
            
        }
        private void InitConnection()
        {
            try
            {
                TcpClient client = new TcpClient();
                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);

                client.Connect(serverEndPoint);

                NetworkStream clientStream = client.GetStream();
                this.clientStream = clientStream;
                Thread listener = new Thread(Listen);
                listener.Start();

            }
            catch(Exception e)
            {
                MessageBox.Show("Connection to the server has failed. Exiting..");
                this.Close();
            }

        }

        private void Listen()
        {
            do
            {
                byte[] bufferIn = new byte[4];
                int bytesRead = clientStream.Read(bufferIn, 0, 4);
                string input = new ASCIIEncoding().GetString(bufferIn);

                if (int.Parse(input[0].ToString()) == 0)
                {
                    begun = true;
                    Invoke((MethodInvoker)delegate
                    { ToggleAll(true);
                       
                        GenerateCards();
                    });

                   
                }

            } while (!begun);

           // try
         //   {
                while (true)
                {
                    byte[] bufferIn = new byte[4];
                    int bytesRead = clientStream.Read(bufferIn, 0, 4);
                    string input = new ASCIIEncoding().GetString(bufferIn);

                    if (input[0] != '\0')
                    {
                    switch (int.Parse(input[0].ToString()))
                    {
                        case 1:
                            switch (AnalyzeTurn(input.Substring(1)))
                            {
                                case 1:
                                    Invoke((MethodInvoker)delegate
                                    { lbl_score1.Text = (int.Parse(lbl_score1.Text) + 1).ToString(); });
                                    break;

                                case -1:
                                    Invoke((MethodInvoker)delegate
                                    { lbl_score2.Text = (int.Parse(lbl_score2.Text) + 1).ToString(); });

                                    break;
                            }
                            break;
                        case 2:
                            Invoke((MethodInvoker)delegate
                            {
                                int myscore = int.Parse(lbl_score1.Text);
                                int opscore = int.Parse(lbl_score2.Text);

                                if (myscore > opscore)
                                {
                                    MessageBox.Show("Game over!");
                                }

                                else if (myscore < opscore)
                                {
                                    MessageBox.Show("You lost!");
                                }

                                else
                                {
                                    MessageBox.Show("It`s a tie!");
                                }
                                ToggleAll(false);
                            });
                            return;
                    }

                }
            }
                  
            //}

 



        }


        public int AnalyzeTurn(string turn)
        {
            int enemyVal = AnalyzeCard(turn);
            while(!sent)
            {

            }

            sent = false;
            if(enemyVal < currentValue )
            {
                return 1;
            }

            if(enemyVal > currentValue)
            {
                return -1;
            }

            return 0;


        }

        public int AnalyzeCard(string turn)
        {
            int val = int.Parse(turn.Substring(0, 2));

            if(val == 1)
            {
                val = 14;
            }

            return val;
        }

        private void GenerateCards()
        {
            Point nextLocation = new System.Drawing.Point(BEGIN, 351);

            for (int i = 0; i < 10; i++)
            {
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "card_" + i;
                currentPic.Image = global::WarZone.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(this.Width / 12, 110);
                currentPic.SizeMode = PictureBoxSizeMode.StretchImage;
                currentPic.Click += CardClick;

                this.Controls.Add(currentPic);
                this.cards.Add(currentPic);
                nextLocation.X += currentPic.Size.Width + 10;
                if(nextLocation.X > this.Size.Width)
                {
                    nextLocation.X = BEGIN;
                    nextLocation.Y = currentPic.Size.Height + 10;
                }  

            }
        }

        private void CardClick(object sender, EventArgs e)
        {
            Random rand = new Random();
            char[] types = { 'H', 'C', 'S', 'D' };
            int val = rand.Next(1, 14);
            char type = types[rand.Next(types.Length)];
            string valstr = "1";
            currentValue = val;
            ChangePic((PictureBox)sender, val, type);
            ((PictureBox)sender).Click -= CardClick;
            sent = true;

            if (val < 10)
            {
                valstr += "0";
            }

            valstr += val + type.ToString();


            try
            {
                byte[] buffer = new ASCIIEncoding().GetBytes(valstr);
                clientStream.Write(buffer, 0, 4);
                clientStream.Flush();
                

            }
            catch(Exception ex)
            {
                MessageBox.Show("Disconnected from the server.. Exiting..");
                this.Close();
            }

        }

        private void ChangePic(PictureBox card, int num, char type)
        {
            string numstr;
            string typestr;

            switch(num)
            {
                case 1:
                    numstr = "ace";
                    break;
                case 11:
                    numstr = "jack";
                    break;
                case 12:
                    numstr = "queen";
                    break;
                case 13:
                    numstr = "king";
                    break;
                default:
                    numstr = "_" + num.ToString();
                    break;
            }

            switch(type)
            {
                case 'C':
                    typestr = "clubs";
                    break;
                case 'S':
                    typestr = "spades";
                    break;
                case 'H':
                    typestr = "hearts";
                    break;
                case 'D':
                    typestr = "diamonds";
                    break;
                default:
                    typestr = "unknown";
                    break;
            }
            Image resourceSet = (Image) Properties.Resources.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true).GetObject(numstr + "_of_" + typestr);
            card.Image = resourceSet;
        }

        private void Forfeit(object sender, EventArgs e)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes("2000");
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();
            MessageBox.Show("Bye!");
            this.Close();
        }
    }
}
