﻿namespace WarZone
{
    partial class WarWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_forfeit = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbl_yourscore = new System.Windows.Forms.Label();
            this.lbl_score1 = new System.Windows.Forms.Label();
            this.lbl_score2 = new System.Windows.Forms.Label();
            this.lbl_enemyscore = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_forfeit
            // 
            this.btn_forfeit.Location = new System.Drawing.Point(285, 12);
            this.btn_forfeit.Name = "btn_forfeit";
            this.btn_forfeit.Size = new System.Drawing.Size(195, 37);
            this.btn_forfeit.TabIndex = 0;
            this.btn_forfeit.Text = "forfeit";
            this.btn_forfeit.UseVisualStyleBackColor = true;
            this.btn_forfeit.Click += new System.EventHandler(this.Forfeit);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WarZone.Properties.Resources.card_back_blue;
            this.pictureBox1.Location = new System.Drawing.Point(318, 55);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(124, 173);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // lbl_yourscore
            // 
            this.lbl_yourscore.AutoSize = true;
            this.lbl_yourscore.Location = new System.Drawing.Point(12, 24);
            this.lbl_yourscore.Name = "lbl_yourscore";
            this.lbl_yourscore.Size = new System.Drawing.Size(61, 13);
            this.lbl_yourscore.TabIndex = 2;
            this.lbl_yourscore.Text = "Your score:";
            // 
            // lbl_score1
            // 
            this.lbl_score1.AutoSize = true;
            this.lbl_score1.Location = new System.Drawing.Point(79, 24);
            this.lbl_score1.Name = "lbl_score1";
            this.lbl_score1.Size = new System.Drawing.Size(13, 13);
            this.lbl_score1.TabIndex = 3;
            this.lbl_score1.Text = "0";
            // 
            // lbl_score2
            // 
            this.lbl_score2.AutoSize = true;
            this.lbl_score2.Location = new System.Drawing.Point(732, 24);
            this.lbl_score2.Name = "lbl_score2";
            this.lbl_score2.Size = new System.Drawing.Size(13, 13);
            this.lbl_score2.TabIndex = 5;
            this.lbl_score2.Text = "0";
            // 
            // lbl_enemyscore
            // 
            this.lbl_enemyscore.AutoSize = true;
            this.lbl_enemyscore.Location = new System.Drawing.Point(632, 24);
            this.lbl_enemyscore.Name = "lbl_enemyscore";
            this.lbl_enemyscore.Size = new System.Drawing.Size(94, 13);
            this.lbl_enemyscore.TabIndex = 4;
            this.lbl_enemyscore.Text = "Opponent`s score:";
            // 
            // WarWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.lbl_score2);
            this.Controls.Add(this.lbl_enemyscore);
            this.Controls.Add(this.lbl_score1);
            this.Controls.Add(this.lbl_yourscore);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btn_forfeit);
            this.Name = "WarWindow";
            this.Text = "WarWindow";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_forfeit;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbl_yourscore;
        private System.Windows.Forms.Label lbl_score1;
        private System.Windows.Forms.Label lbl_score2;
        private System.Windows.Forms.Label lbl_enemyscore;
    }
}